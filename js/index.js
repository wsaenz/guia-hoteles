$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('#popover-mostrar').popover('show');
    setTimeout(function () {
        $("#popover-mostrar").popover('hide');
    }, 5000);
    $('.carousel').carousel({
        interval: 3000
    });
    setTimeout(function () {
        $("#promo").modal('show');
    }, 5000);

    setTimeout(function () {
        $("#promo").modal('hide');
    }, 15000);

    // Script de ejecución cuando se lanza el modal
    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('El modal de suscribción se está mostrando');
        // Script que cambia el color del boton de envio
        $('#registroBtn1').removeClass('btn1');
        $('#registroBtn1').prop('disabled', true);
    });

    // Script de ejecuión cuando el modal está ejecutado
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('El modal de suscribción se mostó');
    });

    // Script de ejecución cuando se cierra el modal
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('El modal de suscribción se está cerrando');
    });

    // Script de ejecuión cuando el modal está cerrado
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('El modal de suscribción se cerró');

        $('#registroBtn1').addClass('btn1');
        $('#registroBtn1').prop('disabled', false);
    });
});